var gulp = require('gulp'),

    $ = require('gulp-load-plugins')({
        lazy: true,
        camelize: true
    });

//lista de js a serem incluidos
var jsSrcs=[
    './js/vendor/modernizr.js'
    './js/vendor/jquery.js'
    './js/foundation.js'
    './js/vendor/foundation.reveal.js'
    './js/vendor/jquery.flexslider-min.js'
    './js/vendor/foundation.accordion.js'
    './js/vendor/foundation.abide.js'
    './js/vendor/foundation-datepicker.js'
    './js/main.js'
    './js/angular.min.js'
    './js/app.js'

];

//esta tarefa junta todos os .css e retorna um arquivo minificado+hash
gulp.task('css', function () {
    return gulp.src('./css/*.css', {base:'./'})
            //.pipe(sass())
            .pipe($.concat('style.min.css'))
            .pipe($.minifyCss())
            .pipe(gulp.dest('./css/'))
            .pipe($.rev())
            .pipe(gulp.dest('./css/min'))
            .pipe($.rev.manifest({merge:true}))
            .pipe(gulp.dest('./'));
});

//esta tarefa junta todos os .js listados e retorna um arquivo minificado+hash
gulp.task('js', function() {
  return gulp.src( jsSrcs, {base:'./'} )
        .pipe($.uglifyjs('all.min.js'))
        .pipe(gulp.dest('./js/'))
        .pipe($.rev())
        .pipe(gulp.dest('./js/min'))
        .pipe($.rev.manifest({merge:true}))
        .pipe(gulp.dest('./'));
});




gulp.task('replace',['css','js'], function () {
    var manifest=gulp.src("rev-manifest.json");
    //console.log(manifest);
    return gulp.src('header.php')
        .pipe($.revReplace({manifest:manifest,replaceInExtensions:['.php','.js']}))
        .pipe($.rename('header.php'))
        .pipe(gulp.dest('./'))
});


gulp.task('clean', function () {
    return gulp.src(['./css/min/*','./js/min/*'], {read: false})
        .pipe($.clean());
})


//a tarefa default agrupa as tarefas css e js
gulp.task('default', ['replace']);



