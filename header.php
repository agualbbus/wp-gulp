<!doctype html>
<html>

<head>
  <?php if(defined('WP_ENV') && WP_ENV=="local"): ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/foundation.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/style.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/font-awesome.css" />

    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/js/vendor/jquery.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/js/main.js"></script>
  <?php else: ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/style.min.css" />
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ; ?>/js/all.min.js"></script>
    
  <?php endif; ?>
</head>
<body>

